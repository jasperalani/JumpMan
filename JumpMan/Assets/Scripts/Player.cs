﻿using UnityEngine;

public class Player : MonoBehaviour {
    public float jumpPower = 10.0f;
    private float speed = 0f;

    private bool jump;
    private Rigidbody2D rb;

    public AudioSource jumpAudio;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update () {
        if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)) {
            Jump();
            jumpAudio.Play();
        }
	}

    private void FixedUpdate() {
        rb.velocity = new Vector2(speed, rb.velocity.y);
    }

    private void Jump() {
        rb.velocity = new Vector2(rb.velocity.x, jumpPower);
    }
}
