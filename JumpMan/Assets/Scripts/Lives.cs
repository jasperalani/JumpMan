﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Lives : MonoBehaviour {

    public int lives;
    public Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void remLife()
    {
        if(lives > 1)
        {
            lives--;
            text.text = lives.ToString();
        }else
        {
            SceneManager.LoadScene("mainScreen2");
        }
    }
}
