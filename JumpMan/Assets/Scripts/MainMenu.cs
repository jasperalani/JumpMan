﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    private Animator anim;

    public Button startGame;
    public Button options;
    public Button quit;
    public Button back;

    private void Awake() {
        anim = GetComponent<Animator>();

        startGame.onClick.AddListener(() => {
            anim.SetInteger("State", 1);
        });

        quit.onClick.AddListener(() => {
            Application.Quit();
        });

        back.onClick.AddListener(() => {
            anim.SetInteger("State", 0);
        });

    }
}
