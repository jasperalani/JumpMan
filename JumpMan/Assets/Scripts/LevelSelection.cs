﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelection : MonoBehaviour {
    private Button[] buttons;

	void Start () {
        buttons = GetComponentsInChildren<Button>();

        for (int i = 0; i < buttons.Length; i++) {
            // A temporary variable that stores the value of i for each loop.
            int j = i ;

            // Assign functionality to each of our buttons, where 'i' is the index of button we are assigning to.
            buttons[i].onClick.AddListener(() => {
                SceneManager.LoadScene(j+2);
            });
        }
	}
}
